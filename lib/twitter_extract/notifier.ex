defmodule TwitterExtract.Notifier do
  use GenServer

  def start_link(_args) do
    GenServer.start_link(__MODULE__, [], [name: :notifier])
  end

  def init(_args) do
    {:ok, []}
  end

  def subscribe(pid) do
    GenServer.cast(:notifier, {:add, pid})
  end

  def unsubscribe(pid) do
    GenServer.cast(:notifier, {:delete, pid})
  end

  def handle_cast({:add, pid}, state) do
    {:noreply, [pid | state]}
  end

  def handle_cast({:delete, pid}, state) do
    IO.puts "unsubscribing"
    {:noreply, state -- [pid]}
  end

  def handle_cast({:notify, text, tweet}, pids) do
    Enum.each(pids, &GenServer.cast(&1, {:incoming, text, tweet}))
    {:noreply, pids}
  end

end
