defmodule TwitterExtract.Storage.Tweet do
  alias TwitterExtract.Storage.Tweet

  @enforce_keys [:text, :created_at, :user_name]
  defstruct [:text, :created_at, :user_name]

  def create do
    :ets.new(:twitter_state, [:public, :bag, :named_table])
  end

  def insert(tbname, %Tweet{} = tweet) do
    :ets.insert(:twitter_state, {tbname, tweet})
  end

  def list(tbname) do
    # :ets.select(:twitter_state, [{ {:"$1", :"$2"}, [{:"=:=", :"$1", {:const, tb_name}}], [:"$2"] }])
    :ets.select(:twitter_state, [{ {:"$1", :"$2"}, [{:"=:=", :"$1", {:const, tbname}}], [:"$2"] }])
  end

  def count(tbname) do
    # :ets.select_count(:twitter_state, [{ {:"$1", :"$2"}, [{:"=:=", :"$1", {:const, tb_name}}], [true] }])
    :ets.select_count(:twitter_state, [{ {:"$1", :"$2"}, [{:"=:=", :"$1", {:const, tbname}}], [true] }])
  end

end

