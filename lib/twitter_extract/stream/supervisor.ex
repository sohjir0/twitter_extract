defmodule TwitterExtract.Stream.Supervisor do
  use DynamicSupervisor

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def search_text(text) do
    spec = {TwitterExtract.Stream.HashTag, text: text}
    DynamicSupervisor.start_child(__MODULE__, spec)
  end

end
