defmodule TwitterExtract.Stream.HashTag do
  use GenServer, restart: :transient
  alias TwitterExtract.Storage.Tweet

  def start_link([text: text]) do
    GenServer.start_link(__MODULE__, text, name: via_tuple(text))
  end

  def init(text) do
    parent_pid = self()
    spawn_link(fn ->
      stream = ExTwitter.stream_filter([track: text], 20000)
      for tweet <- stream, do: send(parent_pid, {:insert, text, tweet})
      send(parent_pid, {:timeout, text})
    end)
    {:ok, []}
  end

  def via_tuple(name),
    do: {:via, Registry, {Registry.TwitterExtract, name}}

  def handle_info({:insert, text, tweet}, state_data) do
    tweet = %Tweet{text: tweet.text, created_at: tweet.created_at, user_name: tweet.user.screen_name}
    TwitterExtract.Storage.Tweet.insert(text, tweet)
    GenServer.cast(:notifier, {:notify, text, tweet})
    {:noreply, state_data}
  end

  def handle_info({:timeout, _text}, state_data) do
    {:stop, :normal, state_data}
  end

end
