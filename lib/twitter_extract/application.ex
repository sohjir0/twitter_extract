defmodule TwitterExtract.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: TwitterExtract.Worker.start_link(arg)
      # {TwitterExtract.Worker, arg},
      { Registry, keys: :unique, name: Registry.TwitterExtract },
      TwitterExtract.Notifier,
      TwitterExtract.Stream.Supervisor
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    TwitterExtract.Storage.Tweet.create()
    opts = [strategy: :one_for_one, name: TwitterExtract.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
